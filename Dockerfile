FROM ros:iron-ros-base-jammy

# Disable non-free repositories
RUN if [ "$(uname -m)" = "x86_64" ]; then \
        echo "\
deb http://archive.ubuntu.com/ubuntu/ $(lsb_release -cs) main universe\n\
deb http://archive.ubuntu.com/ubuntu/ $(lsb_release -cs)-backports main universe\n\
deb http://archive.ubuntu.com/ubuntu/ $(lsb_release -cs)-updates main universe\n\
deb http://security.ubuntu.com/ubuntu/ $(lsb_release -cs)-security main universe\n\
" > /etc/apt/sources.list; \
    else \
        echo "\
deb http://ports.ubuntu.com/ubuntu-ports/ $(lsb_release -cs) main universe\n\
deb http://ports.ubuntu.com/ubuntu-ports/ $(lsb_release -cs)-backports main universe\n\
deb http://ports.ubuntu.com/ubuntu-ports/ $(lsb_release -cs)-updates main universe\n\
deb http://ports.ubuntu.com/ubuntu-ports/ $(lsb_release -cs)-security main universe\n\
" > /etc/apt/sources.list; \
    fi

# Set up locales
RUN apt-get update && \
    apt-get install -y \
        locales \
        curl \
        gettext-base \
    && rm -rf /var/lib/apt/lists/*
RUN locale-gen en_US en_US.UTF-8; update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
ENV LANG en_US.UTF-8

# Install apt packages
COPY apt-packages /tmp/
RUN apt-get update && \
    apt-get install -y \
        $(cut -d# -f1 </tmp/apt-packages | envsubst) \
    && rm -rf /var/lib/apt/lists/* /tmp/apt-packages

RUN echo 'ALL ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
RUN echo 'Defaults env_keep += "DEBUG ROS_DISTRO"' >> /etc/sudoers

COPY env.sh /etc/profile.d/ade_env.sh
COPY entrypoint /ade_entrypoint

COPY colcon-defaults.yaml /etc/skel/.colcon/defaults.yaml
COPY git-prompt.sh /etc/skel/.git-prompt.sh

ENTRYPOINT ["/ade_entrypoint"]
CMD ["/bin/sh", "-c", "trap 'exit 147' TERM; tail -f /dev/null & wait ${!}"]
